#include <math.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>
using namespace std; 

double sum(int fs, int fe, double fx, const double fA, const double fB, const double fC, const double fD)
{
	double sm=0;
	int i;
	i=fs-1;
	do
	{
		i++;
		sm=sm+(fA+(sin(pow(i,fB)+fx)/(cos((fC*i)*pow(abs(fx),fA/fD)))));
	}
	while (i<=fe);
	return sm;	
}

double mul(int fs, int fe, double fx, const double fA, const double fB, const double fP)
{
	double ml=1;
	int i;
	i=fs-1;
	do
	{
		i++;
		ml=ml*(fA+((pow(tan(i*fP*fx),fB))/(fB*i)));
	}
	while (i<=fe);
	return ml;
}

int main() 
{
	system("cls");
	SetConsoleCP(1251); 
	SetConsoleOutputCP(1251);
	const double A=1, B=2, C=3, D=4, P=3.14, K=sqrt(3), L=0.75;
	int x, is=2, ie=15, js=2, je=7;
	double N,M,E;
	cout<<"������ �������� X = ";
	cin>>x;
	N=A/sin(A/K);
	M=x/atan(L);
	E=N*sum(is,ie,x,A,B,C,D)+M*mul(js,je,x,A,B,P);
	printf(" E = %7.5f \n",E);
	system("pause");
	return 0;
}

