#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
using namespace std;

void CmpSim(int** a, int n)
{
	int k=0;
	bool side = false;
	for (int i = 0; i < n; i++)
    {       for (int j = 0; j < n; j++)
            cout << a[i][j] << " ";
        cout << endl;      
	}
	cout << endl;
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if ((i == j || j==n-i-1) && a[i][j]==1)
				k++;
		}
	}
	if (n%2==0)
	{
		if(k==2*n)
			side=true;
		else
			side=false;
	}
	if (n%2!=0)
	{
		if(k+1==2*n)
			side=true;
		else
			side=false;
	}
	if (side==true)
		cout << "������� ��������" << endl;
	else
		cout << "������� �� ��������" << endl;
}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
    int n;
    bool  sideSim = true; 
    srand((unsigned int)time(0));
    cout << "n = ";
    cin >> n;
    system("cls");
    int **a = new int*[n];
    for (int i = 0; i < n; i++)
    {     a[i] = new int[n];
        for (int j = 0; j < n; j++)
            a[i][j] = (rand() % 90) + 10;     
	}
    CmpSim(a, n);
    for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (i == j || j==n-i-1) 
				a[i][j] = 1;
			else 
                a[i][j] = 0;
        }
	}
	CmpSim(a, n);
    system("pause");
}
